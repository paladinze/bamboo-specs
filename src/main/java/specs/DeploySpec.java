package specs;

import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.api.builders.deployment.Environment;
import com.atlassian.bamboo.specs.api.builders.deployment.ReleaseNaming;
import com.atlassian.bamboo.specs.api.builders.permission.DeploymentPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.EnvironmentPermissions;
import com.atlassian.bamboo.specs.api.builders.permission.PermissionType;
import com.atlassian.bamboo.specs.api.builders.permission.Permissions;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.builders.task.ArtifactDownloaderTask;
import com.atlassian.bamboo.specs.builders.task.CleanWorkingDirectoryTask;
import com.atlassian.bamboo.specs.builders.task.DownloadItem;
import com.atlassian.bamboo.specs.builders.task.DumpVariablesTask;

import static specs.Constants.ADMIN_USER_GROUP_NAME;

public class DeploySpec {

    private String projectName;
    private PlanIdentifier planIdentifier;

    public DeploySpec(String projectName, PlanIdentifier planIdentifier) {
        this.projectName = projectName;
        this.planIdentifier = planIdentifier;
    }

    public Deployment createDeploymentProject() {
        final Deployment deployment = new Deployment(planIdentifier, projectName)
                .releaseNaming(new ReleaseNaming("release-1")
                        .autoIncrement(true))
                .environments(new Environment("UAT")
                                .description("the UAT environment")
                                .tasks(new CleanWorkingDirectoryTask(),
                                        new ArtifactDownloaderTask()
                                                .description("Download release contents")
                                                .artifacts(new DownloadItem()
                                                        .allArtifacts(true)),
                                        new DumpVariablesTask()),
                        new Environment("Production")
                                .tasks(new CleanWorkingDirectoryTask(),
                                        new ArtifactDownloaderTask()
                                                .description("Download release contents")
                                                .artifacts(new DownloadItem()
                                                        .allArtifacts(true)),
                                        new DumpVariablesTask()
                                                .description("dump variables")),
                        new Environment("QA")
                                .description("QA environment")
                                .tasks(new CleanWorkingDirectoryTask(),
                                        new ArtifactDownloaderTask()
                                                .description("Download release contents")
                                                .artifacts(new DownloadItem()
                                                        .allArtifacts(true)),
                                        new DumpVariablesTask()));
        return deployment;
    }

    public DeploymentPermissions deploymentPermission() {
        final DeploymentPermissions deploymentPermission = new DeploymentPermissions(projectName)
                .permissions(new Permissions()
                        .groupPermissions(ADMIN_USER_GROUP_NAME, PermissionType.EDIT, PermissionType.VIEW));
        return deploymentPermission;
    }

    private EnvironmentPermissions getEnvironmentPermissions(String envName) {
        final EnvironmentPermissions environmentPermission1 = new EnvironmentPermissions(projectName)
                .environmentName(envName)
                .permissions(new Permissions()
                        .groupPermissions(ADMIN_USER_GROUP_NAME, PermissionType.EDIT, PermissionType.VIEW, PermissionType.BUILD));
        return environmentPermission1;
    }

    public EnvironmentPermissions environmentPermission1() {
        return getEnvironmentPermissions("UAT");
    }

    public EnvironmentPermissions environmentPermission2() {
        return getEnvironmentPermissions("Production");
    }

    public EnvironmentPermissions environmentPermission3() {
        return getEnvironmentPermissions("QA");
    }

}