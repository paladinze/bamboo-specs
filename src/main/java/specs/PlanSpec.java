package specs;

import com.atlassian.bamboo.specs.api.BambooSpec;
import com.atlassian.bamboo.specs.api.builders.deployment.Deployment;
import com.atlassian.bamboo.specs.api.builders.permission.*;
import com.atlassian.bamboo.specs.api.builders.plan.Job;
import com.atlassian.bamboo.specs.api.builders.plan.Plan;
import com.atlassian.bamboo.specs.api.builders.plan.PlanIdentifier;
import com.atlassian.bamboo.specs.api.builders.plan.Stage;
import com.atlassian.bamboo.specs.api.builders.plan.artifact.Artifact;
import com.atlassian.bamboo.specs.api.builders.plan.branches.BranchCleanup;
import com.atlassian.bamboo.specs.api.builders.plan.branches.PlanBranchManagement;
import com.atlassian.bamboo.specs.api.builders.plan.configuration.ConcurrentBuilds;
import com.atlassian.bamboo.specs.api.builders.project.Project;
import com.atlassian.bamboo.specs.api.builders.repository.VcsChangeDetection;
import com.atlassian.bamboo.specs.builders.repository.git.GitRepository;
import com.atlassian.bamboo.specs.builders.task.*;
import com.atlassian.bamboo.specs.builders.trigger.RepositoryPollingTrigger;
import com.atlassian.bamboo.specs.util.BambooServer;

import static specs.Constants.*;

@BambooSpec
public class PlanSpec {

    public static void main(final String[] args) throws Exception {
        //By default credentials are read from the '.credentials' file.
        BambooServer bambooServer = new BambooServer(BAMBOO_SERVER_URL);

        Plan plan1 = new PlanSpec().createPlan1();
        bambooServer.publish(plan1);
        PlanPermissions planPermission = new PlanSpec().createPlanPermission(plan1.getIdentifier());
        bambooServer.publish(planPermission);

        Plan plan2 = new PlanSpec().createPlan2();
        bambooServer.publish(plan2);
        PlanPermissions planPermission2 = new PlanSpec().createPlanPermission(plan2.getIdentifier());
        bambooServer.publish(planPermission2);

        final DeploySpec deploySpec1 = new DeploySpec("deploy1", plan1.getIdentifier());
        final Deployment deployProj1 = deploySpec1.createDeploymentProject();
        bambooServer.publish(deployProj1);
        bambooServer.publish(deploySpec1.deploymentPermission());
        bambooServer.publish(deploySpec1.environmentPermission1());
        bambooServer.publish(deploySpec1.environmentPermission1());
        bambooServer.publish(deploySpec1.environmentPermission1());

        final DeploySpec deploySpec2 = new DeploySpec("deploy2", plan2.getIdentifier());
        final Deployment deployProj2 = deploySpec2.createDeploymentProject();
        bambooServer.publish(deployProj2);
        bambooServer.publish(deploySpec2.deploymentPermission());
        bambooServer.publish(deploySpec2.environmentPermission1());
        bambooServer.publish(deploySpec2.environmentPermission1());
        bambooServer.publish(deploySpec2.environmentPermission1());
    }

    PlanPermissions createPlanPermission(PlanIdentifier planIdentifier) {
        Permissions permission = new Permissions()
                .groupPermissions(ADMIN_USER_GROUP_NAME, PermissionType.ADMIN, PermissionType.CLONE, PermissionType.EDIT)
                .loggedInUserPermissions(PermissionType.VIEW)
                .anonymousUserPermissionView();
        return new PlanPermissions(planIdentifier.getProjectKey(), planIdentifier.getPlanKey()).permissions(permission);
    }

    Project createProject() {
        return new Project()
                .name(BUILD_PROJECT_NAME)
                .key(BUILD_PROJECT_KEY);
    }

    Plan createPlan1() {
        return new Plan(createProject(), "plan1", "PLAN1")
                .description("plan1: build a git repo")
                .pluginConfigurations(new ConcurrentBuilds())
                .stages(new Stage("Stage 1")
                        .jobs(new Job("Build", "BUILD")
                                .artifacts(new Artifact()
                                        .name("artifact_1")
                                        .copyPattern("*")
                                        .shared(true)
                                        .required(true))
                                .tasks(new VcsCheckoutTask()
                                                .description("Checkout Default Repository")
                                                .checkoutItems(new CheckoutItem().defaultRepository()),
                                        new ScriptTask()
                                                .inlineBody("echo build completed!"))))
                .planRepositories(new GitRepository()
                        .name("repo_1")
                        .url("https://github.com/lujakob/nestjs-realworld-example-app.git")
                        .branch("master")
                        .changeDetection(new VcsChangeDetection()))
                .triggers(new RepositoryPollingTrigger())
                .planBranchManagement(new PlanBranchManagement()
                        .delete(new BranchCleanup())
                        .notificationForCommitters());
    }

    Plan createPlan2() {
        return new Plan(createProject(), "plan2", "PLAN2")
                .description("plan2: build a git repo")
                .pluginConfigurations(new ConcurrentBuilds())
                .stages(new Stage("Stage 1")
                        .jobs(new Job("Build", "BUILD")
                                .artifacts(new Artifact()
                                        .name("artifact_2")
                                        .copyPattern("*")
                                        .shared(true)
                                        .required(true))
                                .tasks(new VcsCheckoutTask()
                                                .description("Checkout Default Repository")
                                                .checkoutItems(new CheckoutItem().defaultRepository()),
                                        new ScriptTask()
                                                .inlineBody("echo build completed!"))))
                .planRepositories(new GitRepository()
                        .name("repo_2")
                        .url("https://github.com/PAIR-code/facets.git")
                        .branch("master")
                        .changeDetection(new VcsChangeDetection()))
                .triggers(new RepositoryPollingTrigger())
                .planBranchManagement(new PlanBranchManagement()
                        .delete(new BranchCleanup())
                        .notificationForCommitters());
    }

}
