package specs;

public final class Constants {
    public static final String BAMBOO_SERVER_URL = "http://localhost:8085";
    public static final String ADMIN_USER_GROUP_NAME = "bamboo-admin";
    public static final String BUILD_PROJECT_NAME = "4KM";
    public static final String BUILD_PROJECT_KEY = "METRIC";
    public static final String DEPLOYMENT_PROJECT_NAME = "4KM_deployment_project";
}
