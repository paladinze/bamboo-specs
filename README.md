# Bamboo Spec Setup


## setup credentials 

- create `.credentials` file
```bash
cp .credentials-example .credentials
```
- change it to match local server credential

## run bamboo spec 
```
mvn -Ppublish-specs
```

or follow the following steps

- find the file `PlanSpec`
- right click on the file, and run `PlanSpec.main()` 